import mtranslate
import os
import time

from telethon import TelegramClient
from telethon import sessions
from telethon import connection
from telethon import events
from telethon.tl.types import PeerUser
from telethon.tl.types import InputPhoneContact
from telethon.tl.functions.contacts import ImportContactsRequest



api_id = os.environ["api_id"].replace("x", "")
api_hash = os.environ["api_hash"]
session = os.environ["session"]

client = TelegramClient(
    sessions.StringSession(bytearray.fromhex(session).decode()),
    api_id,
    api_hash,
    connection=connection.ConnectionTcpMTProxyIntermediate,
    proxy=("Myamazing.dynu.net", 1088, "00000000000000000000000000000000")
)


def translate(text, dest="en"):
    try:
        t = mtranslate.translate(text, dest, "auto")
        if t != text:
            return t
    except Exception:
        return 


@client.on(events.NewMessage())
async def onMessage(event):
    if event.message.message == "t.contact":
        try:
            from_id = event.message.from_id
            peer = await client.get_entity(PeerUser(from_id))
            phone = peer.phone
            contact = InputPhoneContact(
                event.message.from_id,
                phone,
                first_name="User",
                last_name=phone
            )
            await client(ImportContactsRequest([contact]))
            await event.respond("Done. You can now invite me to any group.")
        except Exception as e:
            print(e, flush=True)
            await event.respond(
                "An error occurred. Please make sure that I can read your " +
                "phone number and you're writing me directly (not via a bot " +
                "or a channel)."
            )
        return

    text = event.message.message
    lang = "en"
    if text.startswith("t.") and " " in text:
        lang, text = text.split(None, 1)
        lang = lang[2:]
    else:
        from_id = event.message.from_id
        peer = await client.get_entity(PeerUser(from_id))
        if peer.username == "GroupButler_bot":
            return

    t = translate(text, lang)
    if t is not None:
        await event.respond(t)


@client.on(events.chataction.ChatAction())
async def onChatAction(event):
    if event.user_added and (await event.get_user()).is_self:
        await event.respond(
            "\n".join([
                "__I'm a bot, bleep, bloop!__",
                "",
                "Hello, I'm Google Translate bot. I will translate all " +
                "messages to English. Remove me if that's not what you're " +
                "looking for.",
                "",
                "__Tip: you can use `t.ru Hello` to translate to Russian.__",
                "__Tip: if you remove the me and then decide to reinvite me, " +
                "Telegram will require me to add you as a contact. Send me " +
                "`t.contact` as a direct message and then add me.__",
                "__Tip: Group Butler bot's messages are not translated " +
                "automatically. If you want to translate welcome messages, " +
                "prepend them with 't.en '.__"
            ])
        )


client.start()

client.run_until_disconnected()